import SmartNotesInjection from './smart-notes-injection.js'

class SmartNotes {

    constructor(defaults) {
        // eslint-disable-next-line
        console.log('constructor')
        if(!this.checkDefaults(defaults)) {
            return
        }
        this.defaults = {}
        Object.assign(this.defaults, defaults);
        this.init()
    }

    checkDefaults(defaults){

        // Check default object
        if(typeof defaults !== 'object') {
            // eslint-disable-next-line no-console
            console.info('Default params must be an object')
            return false
        }

        // Check if language is soported language
        if(!defaults.hasOwnProperty('language') ||
        defaults.language != 'ca' &&
        defaults.language != 'es' &&
        defaults.language != 'fr' &&
        defaults.language != 'it' &&
        defaults.language != 'de' &&
        defaults.language != 'nl' &&
        defaults.language != 'en' &&
        defaults.language != 'ru' &&
        defaults.language != 'nl') {
            // eslint-disable-next-line no-console
            console.info('Language '+defaults.language+' is not correct language, init with default language')
            defaults.language = 'es'
        }

        // Breack if has one error
        if(!defaults.hasOwnProperty('secretKey') || defaults.secretKey == ''){
            // eslint-disable-next-line no-console
            console.info('secretKey must be filled')
            return false
        }

        if(!defaults.hasOwnProperty('api')){
            // eslint-disable-next-line no-console
            console.info('api must be filled')
            return false
        }

        if(!defaults.hasOwnProperty('url_smart_notes')){
            // eslint-disable-next-line no-console
            console.info('url_smart_notes must be filled')
            return false
        }

        return true

    }

    init(){
        new SmartNotesInjection(this.defaults)
    }

}

export default SmartNotes
