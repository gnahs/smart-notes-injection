class SmartNotesInjection {

    constructor(defaults) {
        this.defaults = defaults
        // eslint-disable-next-line
        console.log('constructor injection')
        this.initSettings()
        this.initSelectors()
        this.initCrossWindowMessageListeners()
        this.create()
    }

    initSettings(){
        this.settings = {
            srcJs:`${this.defaults.api}webscripts/v2/smart-notes-with-dependencies.min.js`,
            srcStyles:`${this.defaults.api}webscripts/v2/smart-notes.min.css`,
            //srcJs:`http://rho-smart-notes.locale/dist/smart-notes-with-dependencies.min.js`,
            //srcStyles:`http://rho-smart-notes.locale/dist/css/smart-notes.min.css`,
            isMobile: document.body.clientWidth < 768
        }
    }

    initSelectors(){
        const iframeName = 'rho-iframe-smart-notes'
        this.selectors = {
            body: document.querySelector('body'),
            iFrame:null,
            iFrameName: iframeName,
            iFrameId: iframeName
        }
    }

    create(){
        // eslint-disable-next-line
        console.log('create')
        this.injectIFrame().then((id)=>{
            // eslint-disable-next-line
            console.log('injected')
            this.selectors.iFrame = document.querySelector('#'+id)
            this.injectMetas()
            this.injectJs()
            this.injectCss()
            this.injectExtraCss()
        })
    }

    // INJECT IFRAME FUNCTIONS
    injectIFrame() {
        var iFrame = document.createElement("iframe")
        iFrame.setAttribute("name", this.selectors.iFrameName)
        iFrame.setAttribute("id", this.selectors.iFrameId)
        iFrame.setAttribute("width", "99%")
        iFrame.setAttribute("style", "height: 160px;")
        iFrame.setAttribute("frameborder", "0")
        iFrame.setAttribute("allowtransparency", "true")
        iFrame.setAttribute("scrolling", "no")
        //iFrame.setAttribute("src", "a")
        this.selectors.body.appendChild(iFrame)

        return new Promise((resolve) => {
            // when the iframe finish loading,
            // resolve with the iframe's identifier.
            window.setTimeout(() => {
                resolve(this.selectors.iFrameId)
            }, 1500)

            // iFrame.onload = function () {
            //     // // eslint-disable-next-line
            //     // console.log('loaded')
            //     // // when the iframe finish loading,
            //     // // resolve with the iframe's identifier.
            //     // resolve(evt.target.getAttribute('id'))
            // }
        })
    }

    injectCss() {
        var styles = this.selectors.iFrame.contentWindow.document.createElement("link")
        styles.rel = "stylesheet"
        styles.href = this.settings.srcStyles
        this.selectors.iFrame.contentWindow.document.getElementsByTagName('head')[0].appendChild(styles)
    }

    injectCustomStyles(notes){
        var style = this.selectors.iFrame.contentWindow.document.createElement("style")
        let styles = this.cutomStyle(notes)
        style.appendChild(this.selectors.iFrame.contentWindow.document.createTextNode(styles))
        this.selectors.iFrame.contentWindow.document.getElementsByTagName('head')[0].appendChild(style)
    }

    injectExtraCss(){
        var style = document.createElement("style")
        let styles = this.stylesheet()
        style.appendChild(document.createTextNode(styles))
        document.getElementsByTagName('head')[0].appendChild(style)
    }

    stylesheet(){
        return `#rho-iframe-smart-notes{
            position:fixed;
            bottom:0;
            right:0;
            z-index:99999999
        }
        @media (min-width:768px){#rho-iframe-smart-notes{position:fixed;bottom:25px;right:0}`
    }

    cutomStyle(notes){
        let style = '';

        notes.forEach((note)=>{

            if(note.color_background !== null) {
                style = style + `.custom-style-${note.id} .brighttheme {background-color:${note.color_background}}`
            }

            if(note.color_text !== null) {
                style = style + `.custom-style-${note.id} .brighttheme {color:${note.color_text}}`
            }

        })

        return style
    }

    injectMetas(){
        var metas = this.selectors.iFrame.contentWindow.document.createElement("meta");
        metas.content = "width=device-width, initial-scale=1";
        metas.name = "viewport";

        var metasUtf = this.selectors.iFrame.contentWindow.document.createElement("meta");
        metasUtf.charset = "utf-8";

        this.selectors.iFrame.contentWindow.document.getElementsByTagName('head')[0].appendChild(metasUtf);
        this.selectors.iFrame.contentWindow.document.getElementsByTagName('head')[0].appendChild(metas);
    }

    injectJs(){
        let script = this.selectors.iFrame.contentWindow.document.createElement("script");
        script.type = "text/javascript";
        script.src = this.settings.srcJs;
        this.selectors.iFrame.contentWindow.document.getElementsByTagName('body')[0].appendChild(script);

        script.onload = (function(){
            this.callSmartNotes()
        }.bind(this));
    }

    // WHEN IFRAME IS INJECTED CALL SMART NOTES SEARCH API
    callSmartNotes(){

        if(this.defaults.hasOwnProperty('notes') && this.defaults.notes.length) {
            this.injectCustomStyles(this.defaults.notes)
            this.injectSmartNotes(this.defaults.notes)
            return
        }
        // eslint-disable-next-line
        console.log('call smart notes')
        this.load((xhr)=>{
            // eslint-disable-next-line
            console.log('lodades inside')
            let smartNotes = JSON.parse(xhr.responseText)

            if(!smartNotes.length){
                this.deleteIFrame()
                return
            }

            this.injectCustomStyles(smartNotes)
            this.injectSmartNotes(smartNotes)
        })
    }

    deleteIFrame(){
        let element = this.selectors.iFrame
        element.parentNode.removeChild(element)
        delete this.selectors.iFrame
    }

    injectSmartNotes(notes){
        let time = 500
        notes.forEach((note)=>{
            window.setTimeout(()=>{

                if(!note.hasOwnProperty('style')) {
                    note.style = 'custom-notes'
                }

                note.isMobile = this.settings.isMobile
                this.sendMessage(note)

            }, time)
            time += 1000
        })
    }

    load(callback){
        // eslint-disable-next-line
        console.log(this.defaults.url_smart_notes)
        var xhr = new XMLHttpRequest()
        xhr.open('POST', this.defaults.url_smart_notes, true)
        xhr.onreadystatechange = function () {
            if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                callback(xhr)
                return
            }
        }
        xhr.setRequestHeader('Content-Type', 'application/json')
        xhr.send(JSON.stringify(this.defaults))
    }

    // EVENTS
    receivedCrossWindowMessage(ev){
        if(ev.detail.action === 'resize') {
            this.resize(ev.detail)
        }
    }

    resize(option) {

        let width = "100%"
        let height = 120
        let layoutWidth = window.outerWidth

        if (layoutWidth > 768) {
            width = '326px'
            height = Math.round(window.outerHeight / 2)
        }

        if (option.option == "grow") {
            this.selectors.iFrame.style.height = height+'px'
            this.selectors.iFrame.style.width = width
        }

        if (option.option == "shrink") {
            this.selectors.iFrame.style.width = width
            this.selectors.iFrame.style.height = option.height+'px'
        }

        if (option.option == "close") {
            this.selectors.iFrame.style.width = '0px'
            this.selectors.iFrame.style.height = '0px'
        }

    }

    initCrossWindowMessageListeners(){
        window.addEventListener('smart-notes:message', this.receivedCrossWindowMessage.bind(this), false )
    }

    sendMessage(message){
        let iFrame = this.selectors.iFrame.contentWindow.window
        let event = new CustomEvent('smart-notes:message', {detail: message})
        iFrame.dispatchEvent(event)
    }
}

export default SmartNotesInjection
