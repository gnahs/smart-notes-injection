# smart-notes-injection

### The main script injected to third party environments.

Capable to init the RHO frontend, inject some extra CSS and JS, and enable crossWindow messaging between windows.
This script has to deal with many important missions:

- Init repository

## License

Author: Alex alemany
All rights reserved. Copyright 2019 GNA Hotel Solutions