const path = require("path")
const loaders = require('./loaders');
const plugins = require('./plugins');

process.env.NODE_ENV = "production"

module.exports = {
    entry: {
      main:'./src/js/index.js',
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, '../dist')
    },
    mode:process.env.NODE_ENV,
    module: {
        rules: [
            loaders.CSSLoader,
            loaders.JSLoader,
            loaders.ESLintLoader
        ]
    },
    plugins: [
        plugins.StyleLintPlugin,
        plugins.MiniCssExtractPlugin,
    ]
  };